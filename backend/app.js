const express = require('express');
const app = express();
const fs = require('fs');
const path = require('path');
//Autorisations
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  next();
});
//Recupération de la racine du disque
app.get('/api/', (req, res, next) => {
  res.status(200).json(getFiles("/"));
});
//Récupération du dossier transmis en paramètre
app.get('/api/:path*', (req, res, next) => {
  //.replace pour remplacer tout les '%20' par des espaces
  let reqPath = req.url.split("api")[1].replace(/%20/g, " ");

  res.status(200).json(getFiles(reqPath));
});

//Récupération des fichiers du dir
function getFiles(directoryPath) {
  const stuff = [];
  let dir = "";
  let files = fs.readdirSync(directoryPath);
  if (directoryPath.length != 1) {
    directoryPath += "/";
  } else {
    dir = "/"
  }
  //Je filtre les fichiers cachés par l'explorateur de base
  files = files.filter(value => !(value.charAt(0) == '.' || value.charAt(0) == '$'))
  files.forEach(function (file) {
    let isDir;
    //Problemes lorsqu'il n'arrive pas à savoir si le file est un dossier ou non
    try {
      isDir = fs.statSync(directoryPath + "/" + file).isDirectory();
    } catch (e) {
      //Certains fichiers n'ont pas d'extensions, 'fs' n'arrive pas à savoir si c'est un dossier ou non
      isDir = false;
    }

    let parentDir = path.dirname(directoryPath + '/' + file).split(dir + path.basename(path.dirname(directoryPath + '/' + file)))[0];
    //Filtre des fichiers
    stuff.push({
      name: file,
      dir: isDir,
      parent: parentDir,
      fullpath: directoryPath + file
    });

  });
  return stuff;

}

module.exports = app;
